# 📕Documentation: ConfigurationManagementProcess
Process for conducting the activities related to software configuration management, ensuring the completeness and correctness of software Configuration Items.

## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **SourceRepository** : A Source Repository is a Loaded Computer System Copy (e.g., an instance of GitLab installed in a Continuous Integration Server) whose purpose is to handle the changes of an Artifact Copy (e.g., copy of a source code)

## ✒️ Mapping Rules
### SourceRepository
A Source Repository is a Loaded Computer System Copy (e.g., an instance of GitLab installed in a Continuous Integration Server) whose purpose is to handle the changes of an Artifact Copy (e.g., copy of a source code)

#### From project to SourceRepository

* *name* **equal** *name*
* *description* **equal** *description*
* *created_at* **equal** *createdDate*
* *last_activity_at* **equal** *lastupdateDate*
* *default_branch* **equal** *defaultBanchName*
* *forks_count* **equal** *forkCount*
* *readme_url* **equal** *readmeURL*
* *http_url_to_repo* **equal** *repoURL*
* *web_url* **equal** *webURL*
* *topics* **equal** *topic*
* *tag_list* **equal** *tag*
* *id* **equal** *externalId*
* *internal_uuid* **equal** *internalId*




