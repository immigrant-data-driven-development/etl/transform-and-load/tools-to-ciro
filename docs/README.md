# 📕Documentation

## 🌀 Project's Package Model
![Domain Diagram](packagediagram.png)

### 📲 Modules
* **[ConfigurationManagementProcess](./configurationmanagementprocess/)** :Process for conducting the activities related to software configuration management, ensuring the completeness and correctness of software Configuration Items.

