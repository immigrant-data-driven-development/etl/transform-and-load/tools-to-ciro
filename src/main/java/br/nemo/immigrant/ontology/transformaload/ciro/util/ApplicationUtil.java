package br.nemo.immigrant.ontology.transformaload.ciro.util;

import br.nemo.immigrant.ontology.entity.base.models.Application;

public class ApplicationUtil {

    public static Application create(String externalId, String internalId, String application){

        return Application.builder().name(application).
                externalId(externalId).
                internalId(internalId).
                build();


    }

    public static Application create(Application source){

        return Application.builder().name(source.getName()).
                externalId(source.getExternalId()).
                internalId(source.getInternalId()).
                build();


    }
}
