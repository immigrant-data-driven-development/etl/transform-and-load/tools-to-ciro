  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class BranchExceptionNotFound extends RuntimeException{

    public BranchExceptionNotFound(String message) {
        super(message);
    }
}

