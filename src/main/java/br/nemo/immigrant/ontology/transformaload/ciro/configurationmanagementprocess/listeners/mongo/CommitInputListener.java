
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.listeners.mongo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
//@Service
public class CommitInputListener {

    @Autowired
    private MongoTemplate mongoTemplate;

    @KafkaListener(topicPattern = "ontology.cmpo.commit", groupId = "commitInput-cmpo-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();
            Document doc = Document.parse (data);
            mongoTemplate.insert(doc, "input");

        }catch (Exception e ){
            log.error(e.getMessage());
        }
    }
}

