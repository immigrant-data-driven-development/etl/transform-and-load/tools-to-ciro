  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class SourceRepositoryExceptionNotFound extends RuntimeException{

    public SourceRepositoryExceptionNotFound(String message) {
        super(message);
    }
}

