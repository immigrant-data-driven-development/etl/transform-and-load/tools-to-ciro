  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;

  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;
  import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.BranchRepository;
  import br.nemo.immigrant.ontology.entity.spo.artifact.repositories.ArtifactArtifactRepository;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.BranchExceptionNotFound;
  import org.apache.commons.codec.digest.DigestUtils;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.SourceRepositoryApplication;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
  import com.fasterxml.jackson.core.JsonProcessingException;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.JsonUtil;
  import com.fasterxml.jackson.databind.JsonNode;
  import com.fasterxml.jackson.databind.ObjectMapper;

  @Component
  @Transactional
  public class BranchApplication extends ApplicationAbstract  {

  @Autowired
  private BranchRepository repository;

  @Autowired
  private SourceRepositoryApplication sourceRepositoryApplication;

  @Autowired
  private ArtifactArtifactRepository artifactArtifactRepository;


  public Branch create(Branch instance) {
    return this.repository.save(instance);
  }


  public Branch createFromTools(Branch instance, String payload) throws SourceRepositoryExceptionNotFound, JsonProcessingException{

    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode rootNode = objectMapper.readTree(payload);

    String sourceRepositoryExternalID = rootNode.path("project").path("id").asText();
    String sourceRepositoryInternalId = new DigestUtils("SHA3-256").digestAsHex(sourceRepositoryExternalID);

    SourceRepository sourceRepository = this.sourceRepositoryApplication.retrieveByInternalID(sourceRepositoryInternalId);

    Branch branch = this.create(instance);

    ArtifactArtifact artifactArtifact = ArtifactArtifact.builder().artifactfrom(sourceRepository).artifactto(branch).eventDate(branch.getCreatedDate()).build();
    artifactArtifactRepository.save(artifactArtifact);

    return branch;
  }

  public  Branch create( Branch instance, String payload) throws Exception, MongoNotFound {

    return this.create(instance);
  }



  public Branch retrieveByExternalID(String externalID) throws BranchExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByApplicationsExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new BranchExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public Branch retrieveByInternalID(String internalID) throws BranchExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new BranchExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private Branch createInstance(IDProjection projection){
      return  Branch.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean existsByInternalId (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

  public Boolean existsByExternalId (String externalID){
    return this.repository.existsByApplicationsExternalId(externalID);
  }

  public Branch retrieveByBranchNameAndProjectExternalId(String branchName,String projectExternalID) throws BranchExceptionNotFound {
    ArtifactArtifact artifactArtifact = this.artifactArtifactRepository.findByArtifactfromApplicationsExternalIdAndArtifacttoName(projectExternalID,branchName);

    if (artifactArtifact == null){
      throw  new BranchExceptionNotFound(branchName);
    }

      return (Branch)artifactArtifact.getArtifactto();
    }
}
