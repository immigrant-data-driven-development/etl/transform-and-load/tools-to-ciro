  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;


  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ContinuousIntegrationProcess;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ContinuousIntegrationProcessRepository;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.ContinuousIntegrationProcessExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;

  import java.util.ArrayList;
  import java.util.List;
  import java.util.Optional;

  @Component
  @Transactional
  public class ContinuousIntegrationProcessApplication extends ApplicationAbstract  {

  @Autowired
  private ContinuousIntegrationProcessRepository repository;


 public ContinuousIntegrationProcess create(ContinuousIntegrationProcess instance) {
     return this.repository.saveAndFlush(instance);
  }


  public ContinuousIntegrationProcess retrieveByInternalID(String internalID) throws ContinuousIntegrationProcessExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new ContinuousIntegrationProcessExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private ContinuousIntegrationProcess createInstance(IDProjection projection){
      return  ContinuousIntegrationProcess.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean existsByInternalId (String internalID){
    return this.repository.existsByInternalId(internalID);
}


}
