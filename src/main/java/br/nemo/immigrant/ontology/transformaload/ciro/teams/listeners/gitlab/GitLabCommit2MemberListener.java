
package br.nemo.immigrant.ontology.transformaload.ciro.teams.listeners.gitlab;

import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabCommit2ChangeImplementerMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabCommit2MemberMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabProject2SourceRepositoryFromMemberMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.services.PersonService;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.exceptions.PersonExceptionNotFound;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class GitLabCommit2MemberListener {


    @Autowired
    private PersonService service;

    @Autowired
    private GitLabProject2SourceRepositoryFromMemberMapper sourceRepositoryMapper;

    @Autowired
    private GitLabCommit2MemberMapper personMapper;

    @Autowired
    private GitLabCommit2ChangeImplementerMapper changeImplementerMapper;


    private final KafkaTemplate<String, String> kafkaTemplate;
    /** Funcionando **/
    //Criando uma pessoa baseado no commit
    @KafkaListener(topics = "application.gitlab.commit", groupId = "gitlabmembercommit2member-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            this.service.process(payload, personMapper,changeImplementerMapper,sourceRepositoryMapper, true);

        }

        catch (TeamExceptionNotFound e ){

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("type", "TeamExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }

        catch (PersonExceptionNotFound e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "PersonExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }

        catch (SourceRepositoryExceptionNotFound e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "SourceRepositoryExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }

        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }


    }

}
