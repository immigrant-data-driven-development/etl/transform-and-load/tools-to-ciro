    package br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    @Component
    public class GitLabCommit2MemberMapper implements Mapper <Person>{

    public Person map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = StringUtil.check(rootNode.path("author_name").asText());

        String email = StringUtil.check(rootNode.path("author_email").asText());

        String externalId = StringUtil.check(rootNode.path("id").asText());

        String internalId = new DigestUtils("SHA3-256").digestAsHex(name);

        Application application = ApplicationUtil.create(externalId,internalId,"gitlab");

        Person person =  Person.builder().
                                name(name).
                                email(email).
                                internalId(internalId).build();
        person.getApplications().add(application);

        return person;
    }


}
