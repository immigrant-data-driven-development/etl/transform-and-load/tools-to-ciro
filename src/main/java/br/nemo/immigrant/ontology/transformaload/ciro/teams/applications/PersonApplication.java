  package br.nemo.immigrant.ontology.transformaload.ciro.teams.applications;

  import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.PersonRepository;

  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.teams.exceptions.PersonExceptionNotFound;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

  @Component
  @Transactional
  public class PersonApplication extends ApplicationAbstract  {

  @Autowired
  private PersonRepository repository;


  public Person create(Person instance) {
    return this.repository.save(instance);
  }

  public void update (Person person, String email){
      this.repository.updateEmail(person.getId(), email);
  }


  public  Person create( Person instance, String payload) throws Exception, MongoNotFound {

    return this.create(instance);
  }

  public Person retrieveByName(String name) throws PersonExceptionNotFound {
      Optional<IDProjection> result = this.repository.findFirstByName(name);

      IDProjection projection = result.orElseThrow(() -> new PersonExceptionNotFound(name));

      return createInstance(projection);
  }

  public Person retrieveByInternalId(String name) throws PersonExceptionNotFound {
          Optional<IDProjection> result = this.repository.findFirstByInternalId(name);

          IDProjection projection = result.orElseThrow(() -> new PersonExceptionNotFound(name));

          return createInstance(projection);
  }


      public Person retrieveByExternalID(String externalID) throws PersonExceptionNotFound {
      Optional<IDProjection> result = this.repository.findFirstByApplicationsExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new PersonExceptionNotFound(externalID));

      return createInstance(projection);
  }



  private Person createInstance(IDProjection projection){
      return  Person.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean existsByInternalId (String internalID){
      return this.repository.existsByInternalId(internalID);
  }
  public Boolean existsByExternalId (String externalID){
    return this.repository.existsByApplicationsExternalId(externalID);
}

public Boolean existsByEmail(String email) {return  this.repository.existsByEmail(email);}

  

}
