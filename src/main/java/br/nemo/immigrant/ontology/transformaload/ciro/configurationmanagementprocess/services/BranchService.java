
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Commit;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactRelation;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderArtifact;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderArtifactRepository;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.*;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabBranch2BranchMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.applications.PersonApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@RequiredArgsConstructor
@Component
public class BranchService {


    @Autowired
    private BranchApplication branchApplication;

    @Autowired
    private SourceRepositoryApplication sourceRepositoryApplication;

    @Autowired
    private CommitApplication commitApplication;

    @Autowired
    private ArtifactArtifactApplication artifactArtifactApplication;

    @Autowired
    private PersonApplication personApplication;

    @Autowired
    private ChangeImplementerApplication changeImplementerApplication;

    @Autowired
    private ProjectStakeholderArtifactRepository projectStakeholderArtifactRepository;

    public void process(ConsumerRecord<String, String> payload, Mapper<Branch> mapper, Mapper<SourceRepository> sourceRepositoryMapper, Mapper<Commit> commitMapper, Mapper<Person> personMapper, Mapper<ChangeImplementer> changeImplementerMapper) throws SourceRepositoryExceptionNotFound, Exception{


        Branch branch = mapper.map(payload.value());

        SourceRepository sourceRepository = sourceRepositoryMapper.map(payload.value());

        Commit commit = commitMapper.map(payload.value());

        Person person = personMapper.map(payload.value());

        ChangeImplementer changeImplementer = changeImplementerMapper.map(payload.value());

        //relacionar um commit com a branch
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(payload.value());
        LocalDateTime created_at = DateUtil.createLocalDateTimeZ(rootNode.path("commit").path("created_at").asText());

        Boolean exists = sourceRepositoryApplication.existsByInternalId(sourceRepository.getInternalId());

        if (!exists){
            sourceRepositoryApplication.create(sourceRepository);
        }
        else {
            sourceRepository = sourceRepositoryApplication.retrieveByInternalID(sourceRepository.getInternalId());
        }

        exists = branchApplication.existsByInternalId(branch.getInternalId());

        if (!exists){
            branch = branchApplication.createFromTools (branch, payload.value());
        }


        exists = commitApplication.existsByInternalId(commit.getInternalId());

        if (!exists){

            commit = commitApplication.create (commit,payload.value());
            sourceRepository = sourceRepositoryApplication.retrieveByInternalID(sourceRepository.getInternalId());
            //relacionando commit com o projeto
            this.commitApplication.updateCommitSourceRepository(commit, sourceRepository);

        }
        else {
            commit = commitApplication.retrieveByInternalID(commit.getInternalId());
        }

        exists = personApplication.existsByInternalId(person.getInternalId());

        if (!exists){
            personApplication.create(person);
        }
        else{
            person = personApplication.retrieveByInternalId(person.getInternalId());
        }

        exists = changeImplementerApplication.existsByInternalID(changeImplementer.getInternalId());

        if (!exists){
            changeImplementer = changeImplementerApplication.createFromTools(changeImplementer, sourceRepository, person, created_at);
        }
        else {
            changeImplementer = changeImplementerApplication.findByInternalId(changeImplementer.getInternalId());
        }

        //Relacionando uma branch a um commit
        ArtifactArtifact instance = ArtifactArtifact.builder().artifactfrom(branch).artifactto(commit).artifactrelation(ArtifactRelation.RELATED).eventDate(created_at).build();
        this.artifactArtifactApplication.create(instance);

        //Relacionando o commit com um changeImplementer
        ProjectStakeholderArtifact projectStakeholderArtifact = ProjectStakeholderArtifact.builder().artifact(commit).projectstakeholder(changeImplementer).eventDate(created_at).build();
        projectStakeholderArtifactRepository.save(projectStakeholderArtifact);

        //Relacionando o branch com um changeImplementer
        projectStakeholderArtifact = ProjectStakeholderArtifact.builder().artifact(branch).projectstakeholder(changeImplementer).eventDate(created_at).build();
        projectStakeholderArtifactRepository.save(projectStakeholderArtifact);

    }

}
