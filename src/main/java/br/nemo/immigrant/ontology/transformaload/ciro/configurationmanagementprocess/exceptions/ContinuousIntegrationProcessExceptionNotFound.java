  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class ContinuousIntegrationProcessExceptionNotFound extends RuntimeException{

    public ContinuousIntegrationProcessExceptionNotFound(String message) {
        super(message);
    }
}

