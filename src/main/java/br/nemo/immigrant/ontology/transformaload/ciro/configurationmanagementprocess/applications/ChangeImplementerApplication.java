  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;
  import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.*;
  import br.nemo.immigrant.ontology.transformaload.ciro.teams.applications.PersonApplication;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ChangeImplementerRepository;
  import br.nemo.immigrant.ontology.transformaload.ciro.teams.exceptions.PersonExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
  import lombok.extern.slf4j.Slf4j;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.base.projections.IDOnlyProjection;
  import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectPersonStakeholderRepository;
  import com.fasterxml.jackson.databind.JsonNode;
  import com.fasterxml.jackson.databind.ObjectMapper;
  import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderArtifact;
  import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderArtifactRepository;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.SourceRepositoryApplication;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
  import br.nemo.immigrant.ontology.entity.spo.project.repositories.SoftwareProjectRepository;
  import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderSoftwareProjectRepository;
  import br.nemo.immigrant.ontology.entity.base.models.Application;

  import java.time.LocalDateTime;
  import java.util.ArrayList;
  import java.util.List;
  import java.util.Optional;
  import java.util.UUID;
  import org.apache.commons.codec.digest.DigestUtils;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
  import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderSoftwareProject;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamMembershipRepository;
  import br.nemo.immigrant.ontology.entity.eo.teams.models.TeamMembership;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamRepository;
  import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;

  @Component
  @Transactional
  @Slf4j
  public class ChangeImplementerApplication extends ApplicationAbstract  {

  @Autowired
  private ChangeImplementerRepository repository;

  @Autowired
  private ProjectPersonStakeholderRepository projectPersonStakeholderRepository;

  @Autowired
  private ProjectStakeholderArtifactRepository projectStakeholderArtifactRepository;

  @Autowired
  private SoftwareProjectRepository softwareProjectRepository;

  @Autowired
  private SourceRepositoryApplication sourceRepositoryApplication;

  @Autowired
  private ProjectStakeholderSoftwareProjectRepository projectStakeholderSoftwareProjectRepository;

  @Autowired
  private PersonApplication personApplication;

  @Autowired
  private TeamMembershipRepository teamMembershipRepository;
  @Autowired
  private TeamRepository teamRepository;

  public ChangeImplementer create(ChangeImplementer instance) {
    return this.repository.save(instance);
  }


  public ChangeImplementer createFromCommit( ChangeImplementer instance, Person person, String payload, LocalDateTime createdDate) throws Exception, TeamExceptionNotFound, MongoNotFound, PersonExceptionNotFound, SourceRepositoryExceptionNotFound {

      ObjectMapper objectMapper = new ObjectMapper();

      JsonNode rootNode = objectMapper.readTree(payload);

      String repositoryExternalId = rootNode.path("project_id").asText();

      //Buscando um repositorio
      SourceRepository sourceRepository = sourceRepositoryApplication.retrieveByExternalID(repositoryExternalId);

      instance.setPerson(person);

      instance = this.create(instance);

      ProjectStakeholderArtifact projectStakeholderArtifact = ProjectStakeholderArtifact.builder().projectstakeholder(instance).artifact(sourceRepository).eventDate(createdDate).build();

      projectStakeholderArtifactRepository.save(projectStakeholderArtifact);

      Optional<IDProjection> result = softwareProjectRepository.findByApplicationsExternalId(repositoryExternalId);

      IDProjection projection = result.orElseThrow(() -> new ChangeImplementerExceptionNotFound(repositoryExternalId));

      SoftwareProject softwareproject = SoftwareProject.builder().id(projection.getId()).build();

      ProjectStakeholderSoftwareProject projectStakeholderSoftwareProject = ProjectStakeholderSoftwareProject.builder().projectstakeholder(instance).softwareproject(softwareproject).build();
      projectStakeholderSoftwareProjectRepository.save(projectStakeholderSoftwareProject);

      return instance;


    }

    public  ChangeImplementer createFromTools( ChangeImplementer instance, SourceRepository sourceRepository, Person person, LocalDateTime createdDate) throws Exception, TeamExceptionNotFound, PersonExceptionNotFound, SourceRepositoryExceptionNotFound {

      instance.setPerson(person);

      instance = this.create(instance);

      ProjectStakeholderArtifact projectStakeholderArtifact = ProjectStakeholderArtifact.builder().projectstakeholder(instance).artifact(sourceRepository).eventDate(createdDate).build();

      projectStakeholderArtifactRepository.save(projectStakeholderArtifact);

      Optional<IDProjection> result = softwareProjectRepository.findByInternalId(sourceRepository.getInternalId());

      IDProjection projection = result.orElseThrow(() -> new SourceProjectExceptionNotFound(sourceRepository.getInternalId()));

      SoftwareProject softwareproject = SoftwareProject.builder().id(projection.getId()).build();

      ProjectStakeholderSoftwareProject projectStakeholderSoftwareProject = ProjectStakeholderSoftwareProject.builder().projectstakeholder(instance).softwareproject(softwareproject).build();

      projectStakeholderSoftwareProjectRepository.save(projectStakeholderSoftwareProject);

      return instance;


    }



  public  ChangeImplementer createFromTools( ChangeImplementer instance, Boolean commit, String payload) throws Exception, TeamExceptionNotFound, PersonExceptionNotFound, SourceRepositoryExceptionNotFound {
    
    ObjectMapper objectMapper = new ObjectMapper();

    JsonNode rootNode = objectMapper.readTree(payload);

    String repositoryExternalId = rootNode.path("project").path("id").asText();

    String repostitotryInternalID = new DigestUtils("SHA3-256").digestAsHex(repositoryExternalId);

    LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("created_at").asText());

    String name = "";

    if (commit){
      name = rootNode.path("author_name").asText();
    }
    else{
      name = rootNode.path("name").asText();
    }


    String internalid = new DigestUtils("SHA3-256").digestAsHex(name);

    //Buscando uma pessoa

    Person person = personApplication.retrieveByInternalId(internalid);

    //Buscando um repositorio

    SourceRepository sourceRepository = sourceRepositoryApplication.retrieveByInternalID(repostitotryInternalID);

    instance.setPerson(person);

    instance = this.create(instance);

    ProjectStakeholderArtifact projectStakeholderArtifact = ProjectStakeholderArtifact.builder().projectstakeholder(instance).artifact(sourceRepository).eventDate(createdDate).build();

    projectStakeholderArtifactRepository.save(projectStakeholderArtifact);

    Optional<IDProjection> result = softwareProjectRepository.findByInternalId(repostitotryInternalID);

    IDProjection projection = result.orElseThrow(() -> new SourceProjectExceptionNotFound(repostitotryInternalID));

    SoftwareProject softwareproject = SoftwareProject.builder().id(projection.getId()).build();

    ProjectStakeholderSoftwareProject projectStakeholderSoftwareProject = ProjectStakeholderSoftwareProject.builder().projectstakeholder(instance).softwareproject(softwareproject).build();

    projectStakeholderSoftwareProjectRepository.save(projectStakeholderSoftwareProject);

    return instance;


  }

  private Team retrieveTeamByExternalId(String externalID) throws TeamExceptionNotFound {

    log.info ("Search Team {}", externalID);

    Optional<IDProjection> result = teamRepository.findFirstByApplicationsExternalId(externalID);

    IDProjection projection = result.orElseThrow(() -> new TeamExceptionNotFound(externalID));

    return Team.builder().id(projection.getId()).build();
  }

  public Boolean exists (String personExternalID, String projectExternalID){

    return this.projectPersonStakeholderRepository.existsByPersonApplicationsExternalIdAndProjectstakeholderartifactArtifactApplicationsExternalId(personExternalID,projectExternalID);
  }

    public Boolean existsByInternalID (String internalID){

      return this.projectPersonStakeholderRepository.existsByInternalId(internalID);
    }


    public Boolean existsByInternalID (String personInternalID, String projectExternalID){

      return this.projectPersonStakeholderRepository.existsByInternalIdAndProjectstakeholderartifactArtifactApplicationsExternalId(personInternalID,projectExternalID);
    }

    private ChangeImplementer createInstance(IDOnlyProjection projection){
      return  ChangeImplementer.builder().id(projection.getId()).build();
    }

  public ChangeImplementer retrieveByEmailAndProjectId(String authoremail,String projectID) throws ChangeImplementerExceptionNotFound {

    Optional<IDOnlyProjection> result = this.projectPersonStakeholderRepository.findByPersonEmailAndProjectstakeholdersoftwareprojectSoftwareprojectApplicationsExternalId(authoremail,projectID);

    IDOnlyProjection projection = result.orElseThrow(() -> new ChangeImplementerExceptionNotFound(authoremail+" - "+projectID));

    return createInstance(projection);

  }

    public ChangeImplementer findByInternalId(String internalID) throws ChangeImplementerExceptionNotFound {

      Optional<IDOnlyProjection> result = this.projectPersonStakeholderRepository.findByInternalId(internalID);

      IDOnlyProjection projection = result.orElseThrow(() -> new ChangeImplementerExceptionNotFound(internalID));

      return createInstance(projection);

    }
    public ChangeImplementer retrieveByChangeImplementerInternalIdAndProjectId(String internalID,String projectID) throws ChangeImplementerExceptionNotFound {

      Optional<IDOnlyProjection> result = this.projectPersonStakeholderRepository.findByInternalIdAndProjectstakeholdersoftwareprojectSoftwareprojectApplicationsExternalId(internalID,projectID);

      IDOnlyProjection projection = result.orElseThrow(() -> new ChangeImplementerExceptionNotFound(internalID+" - "+projectID));

      return createInstance(projection);

    }


}
