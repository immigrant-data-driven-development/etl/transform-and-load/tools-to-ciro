  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class ArtifactCopyExceptionNotFound extends RuntimeException{

    public ArtifactCopyExceptionNotFound(String message) {
        super(message);
    }
}

