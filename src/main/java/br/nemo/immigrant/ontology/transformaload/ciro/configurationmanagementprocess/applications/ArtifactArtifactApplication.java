  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;

  import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
  import br.nemo.immigrant.ontology.entity.spo.artifact.repositories.ArtifactArtifactRepository;
  import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.BranchExceptionNotFound;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;


  import com.fasterxml.jackson.core.JsonProcessingException;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.JsonUtil;
  import com.fasterxml.jackson.databind.JsonNode;
  import com.fasterxml.jackson.databind.ObjectMapper;

  @Component
  @Transactional
  public class ArtifactArtifactApplication extends ApplicationAbstract  {

  @Autowired
  private ArtifactArtifactRepository repository;


  public ArtifactArtifact create(ArtifactArtifact instance) {
    return this.repository.save(instance);
  }
  public Boolean exists (Artifact artifactfrom, Artifact artifactto ){
    return this.repository.existsByArtifactfromAndArtifactto(artifactfrom,artifactto);
  }
  
}
