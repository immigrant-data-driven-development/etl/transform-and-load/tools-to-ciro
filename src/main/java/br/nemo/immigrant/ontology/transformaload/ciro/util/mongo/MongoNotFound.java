package br.nemo.immigrant.ontology.transformaload.ciro.util.mongo;

public class MongoNotFound extends RuntimeException{

  public MongoNotFound(String message) {
      super(message);
  }
}
