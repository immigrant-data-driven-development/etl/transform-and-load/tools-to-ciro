
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository; 
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.SourceRepositoryApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabProject2SourceRepositoryMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class SourceRepositoryService {


    @Autowired
    private SourceRepositoryApplication application;

    public void process(ConsumerRecord<String, String> payload,Mapper<SourceRepository> mapper) throws Exception {


        SourceRepository instance = mapper.map(payload.value());
        Boolean exists = application.existsByInternalId(instance.getInternalId());
        if (!exists){
            application.create (instance);

        }

    }

}
