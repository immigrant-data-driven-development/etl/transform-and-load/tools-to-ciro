    package br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    import java.time.LocalDateTime;

    @Component
    public class GitLabBranch2ChangeImplementerMapper implements Mapper <ChangeImplementer>{

    public ChangeImplementer map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = StringUtil.check(rootNode.path("commit").path("author_name").asText());

        String externalId = StringUtil.check(rootNode.path("commit").path("id").asText());

        String projectName = StringUtil.check(rootNode.path("project").path("name").asText());

        String projectId = StringUtil.check(rootNode.path("project").path("id").asText());

        String internalId = new DigestUtils("SHA3-256").digestAsHex(name+projectId);

        LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("commit").path("created_at").asText());

        Application application = ApplicationUtil.create(externalId,internalId,"gitlab");
        ChangeImplementer changeImplementer = ChangeImplementer.builder().name(name+" : "+projectName).internalId(internalId).createdDate(createdDate).build();
        changeImplementer.getApplications().add(application);

        return changeImplementer;
    }
}
