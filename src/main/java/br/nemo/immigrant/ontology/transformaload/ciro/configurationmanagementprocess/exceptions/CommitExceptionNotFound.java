  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class CommitExceptionNotFound extends RuntimeException{

    public CommitExceptionNotFound(String message) {
        super(message);
    }
}

