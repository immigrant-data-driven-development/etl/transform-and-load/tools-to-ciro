
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.listeners.gitlab.pipeline;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ContinuousIntegrationProcess;
import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.ContinuousIntegrationProcessApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.ProjectApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.SourceRepositoryApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabPipeline2ContinuousIntegrationProcessMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabProject2SourceRepositoryFromPipelineMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class GitLabPipeline2ContinuosIntegrationProcessListener {

    @Autowired
    private GitLabProject2SourceRepositoryFromPipelineMapper projectMapper;

    @Autowired
    private SourceRepositoryApplication sourceRepositoryApplication;

    @Autowired
    private ContinuousIntegrationProcessApplication continuousIntegrationProcessApplication;

    @Autowired
    private ProjectApplication projectApplication;

    @Autowired
    private GitLabPipeline2ContinuousIntegrationProcessMapper continuousIntegrationProcessMapper;
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Transactional
    @KafkaListener(topics = "application.gitlab.pipeline", groupId = "gitlabpipeline2ciprocess-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{
            String data = payload.value();

            SourceRepository sourceRepository = this.createSourceRepository(data);

            this.createContinuousIntegrationProcess(data, sourceRepository);

        }

        catch (Exception e ){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.gitlab.pipeline.error", jsonString);
        }
    }

    @Transactional
    public SourceRepository createSourceRepository (String data) throws Exception {

        SourceRepository sourceRepository = projectMapper.map(data);

        Boolean exists = sourceRepositoryApplication.existsByInternalId(sourceRepository.getInternalId());

        if (!exists) {

            sourceRepository = sourceRepositoryApplication.create(sourceRepository);
        }
        else{
            sourceRepository = sourceRepositoryApplication.retrieveByInternalID(sourceRepository.getInternalId());
        }

        return sourceRepository;
    }

    @Transactional
    public ContinuousIntegrationProcess createContinuousIntegrationProcess(String data, SourceRepository sourceRepository ) throws Exception {
        ContinuousIntegrationProcess continuousIntegrationProcess =  continuousIntegrationProcessMapper.map(data);

        Boolean exists = continuousIntegrationProcessApplication.existsByInternalId(continuousIntegrationProcess.getInternalId());

        if (!exists) {
            continuousIntegrationProcess.setSourcerepository(sourceRepository);
            continuousIntegrationProcessApplication.create(continuousIntegrationProcess);
        }

        return continuousIntegrationProcess;
    }
}
