    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers;

    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ArtifactCopy;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    import java.math.BigDecimal;

    @Component
    public class GitLabRepositoryTree2ArtifactCopyMapper implements Mapper <ArtifactCopy>{

    public ArtifactCopy map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = StringUtil.check(rootNode.path("name").asText());

        String externalId = StringUtil.check(rootNode.path("id").asText());

        String path = StringUtil.check(rootNode.path("path").asText());

        String type = StringUtil.check(rootNode.path("type").asText());
        String sha = "";
        BigDecimal size = new BigDecimal(0);

        if (type.equals("blob")){
            sha = StringUtil.check(rootNode.path("file_info").path("sha").asText());
            size= new BigDecimal(StringUtil.check(rootNode.path("file_info").path("size").asText()));

        }

        String project = StringUtil.check(rootNode.path("project").path("id").asText());


        String internalId;

        internalId = new DigestUtils("SHA3-256").digestAsHex(path.concat(project));

        Application application = ApplicationUtil.create(externalId,internalId,"gitlab");

        ArtifactCopy artifactCopy = ArtifactCopy.builder().
                name(name).
                path(path).
                sha(sha).
                size(size).
                internalId(internalId).build();

        artifactCopy.getApplications().add(application);

        return artifactCopy;
    }
}
