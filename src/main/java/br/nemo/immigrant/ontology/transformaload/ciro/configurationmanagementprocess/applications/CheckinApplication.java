  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;

  import  br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
  import  br.nemo.immigrant.ontology.entity.cmpo.checkin.repositories.CheckinRepository;

  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.CheckinExceptionNotFound;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
  import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;
  import  br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SpecificProcessExceptionNotFound;
  import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessActivity;
  import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessActivityRepository;
  import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessRepository;
  @Component
  @Transactional
  public class CheckinApplication extends ApplicationAbstract  {

  @Autowired
  private CheckinRepository repository;

      @Autowired
  private SpecificProjectProcessRepository specificProjectProcessRepository;

      @Autowired
      private SpecificProjectProcessActivityRepository specificProjectProcessActivityRepository;

  private Checkin create(Checkin instance) {

      return this.repository.save(instance);
  }


  public  Checkin create( Checkin instance, String projectExternalID) throws Exception, MongoNotFound, SpecificProcessExceptionNotFound {

    instance = this.create(instance);
    SpecificProjectProcess specificProjectProcess = retrieveSpecificProjectProcessRepositoryByExternalID (projectExternalID);

    SpecificProjectProcessActivity specificProjectProcessActivity = SpecificProjectProcessActivity.builder().activity(instance).eventDate(instance.getCreatedDate()).specificprojectprocess(specificProjectProcess).build();

    specificProjectProcessActivityRepository.save(specificProjectProcessActivity);

    return instance;
  }

  public SpecificProjectProcess  retrieveSpecificProjectProcessRepositoryByExternalID(String externalID) throws SpecificProcessExceptionNotFound {
          Optional<IDProjection> result = this.specificProjectProcessRepository.findByGeneralprojectprocessSoftwareprojectApplicationsExternalId(externalID);

          IDProjection projection = result.orElseThrow(() -> new SpecificProcessExceptionNotFound (externalID));

          return SpecificProjectProcess.builder().id(projection.getId()).build();
  }

  public Checkin retrieveByExternalID(String externalID) throws CheckinExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByApplicationsExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new CheckinExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public Checkin retrieveByInternalID(String internalID) throws CheckinExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new CheckinExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private Checkin createInstance(IDProjection projection){
      return  Checkin.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean exists (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

  public Boolean existsbyExternalId (String externalId){
    return this.repository.existsByApplicationsExternalId(externalId);
  }

  public Boolean existByInternalId(String internalId){

      return this.repository.existsByInternalId(internalId);
  }

}
