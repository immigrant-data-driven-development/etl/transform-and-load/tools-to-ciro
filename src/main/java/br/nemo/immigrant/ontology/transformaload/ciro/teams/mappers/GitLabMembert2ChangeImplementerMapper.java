    package br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers;

    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import org.springframework.stereotype.Component;
    import java.time.LocalDateTime;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    @Component
    public class GitLabMembert2ChangeImplementerMapper  implements Mapper <ChangeImplementer>{

    public ChangeImplementer map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = StringUtil.check(rootNode.path("name").asText());

        String externalid = StringUtil.check(rootNode.path("id").asText());

        String projectName = StringUtil.check(rootNode.path("project").path("name").asText());

        String project_id = StringUtil.check(rootNode.path("project").path("id").asText());

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name+project_id);

        LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("created_at").asText());

        Application application = ApplicationUtil.create(externalid,internalid,"gitlab");
        ChangeImplementer changeImplementer = ChangeImplementer.builder().name(name+" : "+projectName).internalId(internalid).createdDate(createdDate).build();
        changeImplementer.getApplications().add(application);

        return changeImplementer;
    }
}
