    package br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers;

    import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;
    import java.time.LocalDateTime;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    @Component
    public class GitLabMember2MemberMapper  implements Mapper <Person>{

    public Person map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String web_url = StringUtil.check(rootNode.path("web_url").asText());

        String username = StringUtil.check(rootNode.path("username").asText());

        String name = StringUtil.check(rootNode.path("name").asText());

        LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("created_at").asText());

        String externalid = StringUtil.check(rootNode.path("id").asText());

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name);

        Application application = ApplicationUtil.create(externalid,internalid,"gitlab");

        Person person =  Person.builder().description("weburl:"+web_url+";username="+username).
                                name(name).
                                createdDate(createdDate).
                                internalId(internalid).build();
        person.getApplications().add(application);

        return person;
    }


}
