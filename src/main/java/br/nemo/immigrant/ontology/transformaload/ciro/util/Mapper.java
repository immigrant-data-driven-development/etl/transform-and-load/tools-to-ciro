package br.nemo.immigrant.ontology.transformaload.ciro.util;

public interface Mapper <T> {
  public T map (String element) throws Exception;
}
