  package br.nemo.immigrant.ontology.transformaload.ciro.teams.exceptions;

  public class PersonExceptionNotFound extends RuntimeException{

    public PersonExceptionNotFound(String message) {
        super(message);
    }
}

