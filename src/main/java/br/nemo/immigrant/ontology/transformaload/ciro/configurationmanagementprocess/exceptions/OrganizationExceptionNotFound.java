  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class OrganizationExceptionNotFound extends RuntimeException{

    public OrganizationExceptionNotFound(String message) {
        super(message);
    }
}

