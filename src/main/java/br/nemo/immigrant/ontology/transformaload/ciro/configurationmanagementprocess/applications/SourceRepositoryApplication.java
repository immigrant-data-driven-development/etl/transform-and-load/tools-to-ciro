  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;

  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
  
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;

  import java.util.ArrayList;
  import java.util.List;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository; 
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.SourceRepositoryRepository;
  import br.nemo.immigrant.ontology.entity.base.models.Application;

  import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
  import br.nemo.immigrant.ontology.entity.spo.project.repositories.SoftwareProjectRepository;


  import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessArtifact;
  import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessArtifactRepository;

  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfiguationManagementProcess;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConfiguationManagementProcessRepository;

  import br.nemo.immigrant.ontology.entity.spo.process.models.GeneralProjectProcess;
  import br.nemo.immigrant.ontology.entity.spo.process.repositories.GeneralProjectProcessRepository;

  import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamRepository;

  import java.util.List;

  @Component
  @Transactional
  public class SourceRepositoryApplication extends ApplicationAbstract  {

  @Autowired
  private SourceRepositoryRepository repository;

  @Autowired
  private SoftwareProjectRepository softwareProjectRepository;

  @Autowired
  private GeneralProjectProcessRepository generalProjectProcessRepository;

  @Autowired
  private ConfiguationManagementProcessRepository configuationManagementProcessRepository;

  @Autowired
  private SpecificProjectProcessArtifactRepository specificProjectProcessArtifactRepository;

  @Autowired
  private TeamRepository teamRepository;

  public SourceRepository create(SourceRepository instance) {


    List<Application> applications = new ArrayList<>(instance.getApplications());

    Application projectApplication = ApplicationUtil.create(applications.get(0));
    Application generalProcessApplication = ApplicationUtil.create(applications.get(0));
    Application specificProcessApplication = ApplicationUtil.create(applications.get(0));
    Application teamApplication = ApplicationUtil.create(applications.get(0));

    //Criar o Projeto
    SoftwareProject softwareProject  = SoftwareProject.builder().name(instance.getName()).createdDate(instance.getCreatedDate()).internalId(instance.getInternalId()).build();
    softwareProject.getApplications().add(projectApplication);
    softwareProject = softwareProjectRepository.saveAndFlush(softwareProject);

    //Criar o Processo Generico
    GeneralProjectProcess generalProjectProcess = GeneralProjectProcess.builder().softwareproject(softwareProject).name(instance.getName()).internalId(instance.getInternalId()).build();
    generalProjectProcess.getApplications().add(generalProcessApplication);
    generalProjectProcess = generalProjectProcessRepository.saveAndFlush(generalProjectProcess);

    ConfiguationManagementProcess configuationManagementProcess = ConfiguationManagementProcess.builder().generalprojectprocess(generalProjectProcess).name(instance.getName()).internalId(instance.getInternalId()).build();
    configuationManagementProcess.getApplications().add(specificProcessApplication);
    configuationManagementProcess = configuationManagementProcessRepository.saveAndFlush(configuationManagementProcess);

    instance = this.repository.saveAndFlush(instance);

    //Associar o repositorio ao processos especifico
    SpecificProjectProcessArtifact specificProjectProcessArtifact = SpecificProjectProcessArtifact.builder().artifact(instance).specificprojectprocess(configuationManagementProcess).eventDate(instance.getCreatedDate()).build();
    specificProjectProcessArtifactRepository.saveAndFlush(specificProjectProcessArtifact);

    //Criar um team project
    Team team = Team.builder().name(instance.getName()).internalId(instance.getInternalId()).project(softwareProject).build();
    team.getApplications().add(teamApplication);
    teamRepository.save(team);

    return instance;
  }


  public  SourceRepository create( SourceRepository instance, String payload) throws Exception, MongoNotFound {

    return this.create(instance);
  }



  public SourceRepository retrieveByExternalID(String externalID) throws SourceRepositoryExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByApplicationsExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new SourceRepositoryExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public SourceRepository retrieveByInternalID(String internalID) throws SourceRepositoryExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new SourceRepositoryExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private SourceRepository createInstance(IDProjection projection){
      return  SourceRepository.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean existsByInternalId (String internalID){
    return this.repository.existsByInternalId(internalID);
}

public Boolean existsByExternalId (String externalID){
  return this.repository.existsByApplicationsExternalId(externalID);
}

}
