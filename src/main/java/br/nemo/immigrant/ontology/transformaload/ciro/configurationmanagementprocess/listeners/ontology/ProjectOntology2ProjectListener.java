
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.listeners.ontology;

import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.OrganizationExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.ProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.filters.ontology.ProjectOntology2ProjectFilter;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.ontology.ProjectOntology2ProjectMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services.ProjectService;
import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
//@Service
public class ProjectOntology2ProjectListener {


    @Autowired
    private ProjectOntology2ProjectFilter filter;

    @Autowired
    private ProjectOntology2ProjectMapper mapper;

    @Autowired
    private ProjectService service;

    private final KafkaTemplate<String, String> kafkaTemplate;
    @KafkaListener(topicPattern  = "ontology.*.project", groupId = "projectontology2ciroproject-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.processOntology(payload, mapper);
            }

        }catch (OrganizationExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.ciro.project", payload.value());

        }
        catch (ProjectExceptionNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.ciro.project", payload.value());
        }
        catch (MongoNotFound e){
            log.error(e.getMessage());
            kafkaTemplate.send("ontology.ciro.project", payload.value());
        }
        catch (Exception e ){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    @KafkaListener(topics = "ontology.ciro.project", groupId = "projectCIRONothing-group")
    public void consumeNothing(ConsumerRecord<String, String> payload) {
        String value = payload.value();

    }

}
