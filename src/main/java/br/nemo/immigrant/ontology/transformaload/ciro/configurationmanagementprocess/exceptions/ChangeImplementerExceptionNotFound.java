  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class ChangeImplementerExceptionNotFound extends RuntimeException{

    public ChangeImplementerExceptionNotFound(String message) {
        super(message);
    }
}

