  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;
  import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactRelation;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Commit;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.CommitRepository;
  import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
  import br.nemo.immigrant.ontology.entity.eo.teams.repositories.PersonRepository;
  import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.CommitExceptionNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.ArtifactArtifactApplication;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;


  @Component
  @Transactional
  public class CommitApplication extends ApplicationAbstract  {

  @Autowired
  private CommitRepository repository;

   @Autowired
   private ArtifactArtifactApplication artifactArtifactApplication;


  //preciso criar as pessoas quando necessário, se elas nao existem é necessário criar
  public Commit create(Commit instance, String payload ) {

    return this.repository.save(instance);
  }

  public Commit retrieveByExternalID(String externalID) throws CommitExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByApplicationsExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new CommitExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public Commit retrieveByInternalID(String internalID) throws CommitExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new CommitExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private Commit createInstance(IDProjection projection){
      return  Commit.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean existsByInternalId (String internalID){
    return this.repository.existsByInternalId(internalID);
}

  public Boolean existsByExternalId (String externalID){
    return this.repository.existsByApplicationsExternalId(externalID);
  }


  public void updateCommitSourceRepository(Commit commit, SourceRepository sourceRepository){

    ArtifactArtifact instance = ArtifactArtifact.builder().artifactfrom(sourceRepository).artifactto(commit).artifactrelation(ArtifactRelation.RELATED).eventDate(commit.getCreatedDate()).build();
    this.artifactArtifactApplication.create(instance);
  }
}
