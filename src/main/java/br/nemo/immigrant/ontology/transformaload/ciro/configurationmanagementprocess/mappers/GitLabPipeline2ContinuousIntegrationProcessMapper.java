    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ContinuousIntegrationProcess;
    import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
    import br.nemo.immigrant.ontology.entity.spo.process.models.SuccessType;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;


    @Component
    public class GitLabPipeline2ContinuousIntegrationProcessMapper implements Mapper <ContinuousIntegrationProcess>{

    public ContinuousIntegrationProcess map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("web_url").asText();

        String createddate = rootNode.path("created_at").asText();

        String status = rootNode.path("status").asText();

        SuccessType successType = SuccessType.SUCCESSS;

        if (status.equals("failed")){
            successType = SuccessType.UNSUCCESS;
        }

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);

        Application application = ApplicationUtil.create(externalid,internalid,"gitlab");

        ContinuousIntegrationProcess continuousIntegrationProcess = ContinuousIntegrationProcess.builder().
                name(name).
                createdDate(DateUtil.createLocalDateTimeZ(createddate)).
                successtype(successType).
                internalId(internalid).build();

        continuousIntegrationProcess.getApplications().add(application);

        return continuousIntegrationProcess;
    }
}
