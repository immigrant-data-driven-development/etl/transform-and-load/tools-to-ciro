  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class SourceProjectExceptionNotFound extends RuntimeException{

    public SourceProjectExceptionNotFound(String message) {
        super(message);
    }
}

