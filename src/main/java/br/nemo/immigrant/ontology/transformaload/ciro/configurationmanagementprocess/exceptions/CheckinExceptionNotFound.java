  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class CheckinExceptionNotFound extends RuntimeException{

    public CheckinExceptionNotFound(String message) {
        super(message);
    }
}

