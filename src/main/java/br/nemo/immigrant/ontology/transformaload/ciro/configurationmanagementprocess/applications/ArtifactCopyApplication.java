  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;

  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ArtifactCopy;
  import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ArtifactCopyRepository;

  import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationAbstract;
  import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
  import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.ArtifactCopyExceptionNotFound;
  import org.springframework.stereotype.Component;
  import org.springframework.transaction.annotation.Transactional;
  import java.util.Optional;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.bson.Document;
  import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

  @Component
  @Transactional
  public class ArtifactCopyApplication extends ApplicationAbstract  {

  @Autowired
  private ArtifactCopyRepository repository;

  public ArtifactCopy create(ArtifactCopy instance) {
    return this.repository.save(instance);
  }


  public  ArtifactCopy create( ArtifactCopy instance, String payload) throws Exception, MongoNotFound {


    return this.create(instance);
  }



  public ArtifactCopy retrieveByExternalID(String externalID) throws ArtifactCopyExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByApplicationsExternalId(externalID);

      IDProjection projection = result.orElseThrow(() -> new ArtifactCopyExceptionNotFound(externalID));

      return createInstance(projection);
  }

  public ArtifactCopy retrieveByInternalID(String internalID) throws ArtifactCopyExceptionNotFound {
      Optional<IDProjection> result = this.repository.findByInternalId(internalID);

      IDProjection projection = result.orElseThrow(() -> new ArtifactCopyExceptionNotFound(internalID));

      return createInstance(projection);
  }

  private ArtifactCopy createInstance(IDProjection projection){
      return  ArtifactCopy.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
  }

  public Boolean existsByInternalId (String internalID){
      return this.repository.existsByInternalId(internalID);
  }

  public Boolean existsByExternalId (String externalId){
      return this.repository.existsByApplicationsExternalId(externalId);
  }

}
