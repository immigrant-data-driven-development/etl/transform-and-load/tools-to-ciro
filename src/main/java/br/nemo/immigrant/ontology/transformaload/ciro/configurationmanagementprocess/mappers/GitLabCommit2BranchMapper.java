    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;
    import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    import java.time.LocalDateTime;

    @Component
    public class GitLabCommit2BranchMapper implements Mapper <Branch>{

    public Branch map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = StringUtil.check(rootNode.path("branch").path("name").asText());

        boolean merged = Boolean.parseBoolean(rootNode.path("branch").path("merged").asText());

        boolean main = Boolean.parseBoolean(rootNode.path("branch").path("default").asText());

        boolean branchprotected = Boolean.parseBoolean(rootNode.path("branch").path("protected").asText());

        String weburl = StringUtil.check(rootNode.path("branch").path("web_url").asText());

        String project_id = StringUtil.check(rootNode.path("project").path("id").asText());

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name+project_id);


        LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("created_at").asText());

        Application application = ApplicationUtil.create(internalid,internalid,"gitlab");

        Branch branch =  Branch.builder().name(name).
                                merged(merged).
                                main(main).
                                branchprotected(branchprotected).
                                webURL(weburl).
                                createdDate(createdDate).
                                artifacttype(ArtifactType.INFORMATIONITEM).
                                internalId(internalid).build();
        branch.getApplications().add(application);

        return branch;
    }
}
