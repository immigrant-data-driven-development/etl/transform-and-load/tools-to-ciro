    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
    import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;


    @Component
    public class GitLabProject2SourceRepositoryFromPipelineMapper implements Mapper <SourceRepository>{

    public SourceRepository map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("project").path("name").asText();

        String description = rootNode.path("project").path("description").asText();

        String createddate = rootNode.path("project").path("created_at").asText();

        String lastupdatedate = rootNode.path("project").path("last_activity_at").asText();

        String defaultbanchname = rootNode.path("project").path("default_branch").asText();

        String forkcount = rootNode.path("project").path("forks_count").asText();

        String readmeurl = rootNode.path("project").path("readme_url").asText();

        String repourl = rootNode.path("project").path("http_url_to_repo").asText();

        String weburl = rootNode.path("project").path("web_url").asText();

        String topic = rootNode.path("project").path("topics").asText();

        String tag = rootNode.path("project").path("tag_list").asText();

        String externalid = rootNode.path("project").path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);

        Application application = ApplicationUtil.create(externalid,internalid,"gitlab");

        SourceRepository sourceRepository = SourceRepository.builder().name(name).
                description(description).
                createdDate(DateUtil.createLocalDateTimeZ(createddate)).
                updatedDate(DateUtil.createLocalDateTimeZ(lastupdatedate)).
                defaultBranchName(defaultbanchname).
                forkCount(Integer.parseInt(forkcount)).
                readmeURL(readmeurl).
                repoURL(repourl).
                webURL(weburl).
                topic(topic).
                tag(tag).
                artifacttype(ArtifactType.INFORMATIONITEM).
                internalId(internalid).build();

        sourceRepository.getApplications().add(application);

        return sourceRepository;
    }
}
