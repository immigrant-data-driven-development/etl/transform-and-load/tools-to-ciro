
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.listeners.gitlab.sourcerepository;


import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services.SourceRepositoryService;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.filters.GitLabProject2SourceRepositoryFilter;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabProject2SourceRepositoryMapper;
import org.json.simple.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class GitLabProject2SourceRepositoryListener {


    @Autowired
    private GitLabProject2SourceRepositoryFilter filter;

    @Autowired
    private GitLabProject2SourceRepositoryMapper mapper;

    @Autowired
    private SourceRepositoryService service;

    private final KafkaTemplate<String, String> kafkaTemplate;
    /** Funcinonando***/
    /** Criando um respositorio baseado no conceito de projeto do Gitlab **/
    @KafkaListener(topics = "application.gitlab.project", groupId = "gitlabproject2sourcerepository-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{


            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }

        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.gitlab.project.error",jsonString);


        }
    }
}
