
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services;
import br.nemo.immigrant.ontology.entity.base.models.Application;
import br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.*;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactRelation;
import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.ActivityArtifactRepository;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderActivity;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderArtifact;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderActivityRepository;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderArtifactRepository;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.CheckinApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.*;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.applications.PersonApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;

import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@RequiredArgsConstructor
@Component
@Transactional
public class CommitService {

    @Autowired
    private BranchApplication branchApplication;

    @Autowired
    private CommitApplication commitApplication;

    @Autowired
    private SourceRepositoryApplication sourceRepositoryApplication;

    @Autowired
    private ArtifactArtifactApplication artifactArtifactApplication;

    @Autowired
    private PersonApplication personApplication;

    @Autowired
    private ChangeImplementerApplication changeImplementerApplication;

    @Autowired
    private ProjectStakeholderArtifactRepository projectStakeholderArtifactRepository;

    @Autowired
    private ArtifactCopyApplication artifactCopyApplication;

    @Autowired
    private CheckinApplication checkinApplication;

    @Autowired
    private ActivityArtifactRepository activityArtifactRepository;

    @Autowired
    private ProjectStakeholderActivityRepository projectStakeholderActivityRepository;

    public void process(ConsumerRecord<String, String> payload,
                        Mapper<Commit> commitMapper,
                        Mapper<SourceRepository> sourceRepositoryMapper,
                        Mapper<Branch> branchMapper,
                        Mapper<Checkin> checkinMapper,
                        Mapper<Person> personMapper,
                        Mapper<ChangeImplementer> changeImplementerMapper) throws  Exception{


        Commit commit = commitMapper.map(payload.value());
        SourceRepository sourceRepository = sourceRepositoryMapper.map(payload.value());
        Person person = personMapper.map(payload.value());
        ChangeImplementer changeImplementer = changeImplementerMapper.map((payload.value()));
        Branch branch = branchMapper.map((payload.value()));
        Checkin checkin = checkinMapper.map((payload.value()));

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(payload.value());

        LocalDateTime created_at = DateUtil.createLocalDateTimeZ(rootNode.path("created_at").asText());
        String projectID = rootNode.path("project").path("id").asText();

        Boolean exists = commitApplication.existsByInternalId(commit.getInternalId());

        if (!exists){
            commit = commitApplication.create (commit,payload.value());
        }
        else{
            commit = commitApplication.retrieveByInternalID (commit.getInternalId());
        }

        exists = sourceRepositoryApplication.existsByInternalId(sourceRepository.getInternalId());

        if (!exists){
            sourceRepository = sourceRepositoryApplication.create(sourceRepository);
        }
        else {
            sourceRepository = sourceRepositoryApplication.retrieveByInternalID(sourceRepository.getInternalId());
        }

        //relacionando commit com sourceRepository
        exists = this.artifactArtifactApplication.exists(sourceRepository,commit);

        if (!exists){
            this.commitApplication.updateCommitSourceRepository(commit, sourceRepository);
        }

        exists = this.personApplication.existsByInternalId(person.getInternalId());

        if (!exists){
           person = personApplication.create(person);
        }
        else{
            person = personApplication.retrieveByInternalId(person.getInternalId());
        }

        exists = changeImplementerApplication.existsByInternalID(changeImplementer.getInternalId());

        if (!exists){
            changeImplementer = changeImplementerApplication.createFromTools(changeImplementer,sourceRepository,person,created_at);

        }
        else{
            changeImplementer = changeImplementerApplication.findByInternalId(changeImplementer.getInternalId());
        }

        exists = branchApplication.existsByInternalId(branch.getInternalId());

        if (!exists){
            branch = branchApplication.createFromTools (branch, payload.value());
        }
        else{
            branch = branchApplication.retrieveByInternalID(branch.getInternalId());
        }

        exists = checkinApplication.existByInternalId(checkin.getInternalId());
        if (!exists){
           checkin =  checkinApplication.create (checkin,projectID);

        }
        else{
            checkin = checkinApplication.retrieveByInternalID(checkin.getInternalId());
        }
        //relacionando um checking com o um commit
        exists =activityArtifactRepository.existsByArtifactAndActivity(commit,checkin);

        if (!exists){
            LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("created_at").asText());

            ActivityArtifact activityArtifact = ActivityArtifact.builder().artifact(commit).activity(checkin).eventDate(createdDate).build();

            activityArtifactRepository.save(activityArtifact);

        }

        exists = projectStakeholderActivityRepository.existsByProjectstakeholderAndActivity(changeImplementer,checkin);

        if (!exists){


            ProjectStakeholderActivity projectStakeholderActivity = ProjectStakeholderActivity.builder().projectstakeholder(changeImplementer).activity(checkin).eventDate(checkin.getCreatedDate()).build();

            projectStakeholderActivityRepository.save(projectStakeholderActivity);

        }


        exists = artifactArtifactApplication.exists(branch,commit);

        if (!exists){
            //Relacionando uma branch a um commit
            ArtifactArtifact instance = ArtifactArtifact.builder().artifactfrom(branch).artifactto(commit).artifactrelation(ArtifactRelation.RELATED).eventDate(created_at).build();
            this.artifactArtifactApplication.create(instance);
        }


        //relacionando o ProjectStakeholder a Commit
        exists = this.projectStakeholderArtifactRepository.existsByProjectstakeholderAndArtifact(changeImplementer,commit);

        if (!exists){

            ProjectStakeholderArtifact instance = ProjectStakeholderArtifact.builder().projectstakeholder(changeImplementer).artifact(commit).eventDate(created_at).build();

            this.projectStakeholderArtifactRepository.save(instance);

        }

        // criando os artefatos
        JsonNode files =  rootNode.path("diff");

        for (JsonNode fileNode: files){

            String path = fileNode.path("file_path").asText();
            String[] parts = path.split("/");
            String name = fileNode.path("file_path").asText();

            if (parts.length > 0){
                name =  parts[parts.length - 1];
            }
            String status = fileNode.path("status").asText();

            String project = StringUtil.check(rootNode.path("project").path("id").asText());
            String internalId = new DigestUtils("SHA3-256").digestAsHex(path.concat(project));

            Application application = ApplicationUtil.create(internalId,internalId,"gitlab");

            ArtifactCopy artifactCopy = ArtifactCopy.builder().
                    name(name).
                    path(path).
                    createdDate(created_at).
                    internalId(internalId).build();

            artifactCopy.getApplications().add(application);

            exists = artifactCopyApplication.existsByInternalId(artifactCopy.getInternalId());

            if (!exists){
                artifactCopy = artifactCopyApplication.create (artifactCopy);

            }
            else {
                artifactCopy = artifactCopyApplication.retrieveByInternalID(artifactCopy.getInternalId());
            }

            //relacionando com o sourceRepository

            exists = this.artifactArtifactApplication.exists(sourceRepository,artifactCopy);

            if (!exists){

                ArtifactArtifact instance = ArtifactArtifact.builder().artifactfrom(sourceRepository).artifactto(artifactCopy).artifactrelation(ArtifactRelation.RELATED).eventDate(created_at).build();

                this.artifactArtifactApplication.create(instance);

            }

            //relacionando o artifact copy com o changeImplementer
            exists = this.projectStakeholderArtifactRepository.existsByProjectstakeholderAndArtifact(changeImplementer,artifactCopy);

            if (!exists){

                ProjectStakeholderArtifact instance = ProjectStakeholderArtifact.builder().projectstakeholder(changeImplementer).artifact(artifactCopy).eventDate(created_at).build();

                this.projectStakeholderArtifactRepository.save(instance);

            }
            // relacionando com commit e artifact copy
            //Voltar e falar se foi created, usage, deleted
            exists = this.artifactArtifactApplication.exists(commit,artifactCopy);

            if (!exists){

                ArtifactArtifact instance = ArtifactArtifact.builder().artifactfrom(commit).artifactto(artifactCopy).artifactrelation(ArtifactRelation.RELATED).eventDate(created_at).build();

                this.artifactArtifactApplication.create(instance);

            }
            // relacionando com branch e artifact copy
            //Voltar e falar se foi created, usage, deleted
            exists = this.artifactArtifactApplication.exists(branch,artifactCopy);
            if (!exists){

                ArtifactArtifact instance = ArtifactArtifact.builder().artifactfrom(branch).artifactto(artifactCopy).artifactrelation(ArtifactRelation.RELATED).eventDate(created_at).build();

                this.artifactArtifactApplication.create(instance);

            }
        }


    }

    

}
