    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers;

    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Commit;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;
    import java.time.LocalDateTime;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import org.apache.commons.codec.digest.DigestUtils;

    @Component
    public class GitLabBranchCommit2CommitMapper  implements Mapper <Commit>{

    public Commit map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String weburl = StringUtil.check(rootNode.path("commit").path("web_url").asText());

        String trailers = StringUtil.check(rootNode.path("commit").path("trailers").asText());

        String name = StringUtil.check(rootNode.path("commit").path("title").asText());

        String message = StringUtil.check(rootNode.path("commit").path("message").asText());

        LocalDateTime commiteddate = DateUtil.createLocalDateTimeZ(rootNode.path("commit").path("committed_date").asText());

        LocalDateTime createddate =  DateUtil.createLocalDateTimeZ(rootNode.path("commit").path("created_at").asText());

        String shortid = StringUtil.check(rootNode.path("commit").path("short_id").asText());

        String externalid = StringUtil.check(rootNode.path("commit").path("id").asText());

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);

        Application application = ApplicationUtil.create(externalid,internalid,"gitlab");

        Commit commit = Commit.builder().webURL(weburl).
                trailers(trailers).
                name(name).
                message(message).
                commitedDate(commiteddate).
                createdDate(createddate).
                shortId(shortid).
                artifacttype(ArtifactType.INFORMATIONITEM).
                internalId(internalid).build();

        commit.getApplications().add(application);

        return commit;
    }
}
