
package br.nemo.immigrant.ontology.transformaload.ciro.teams.listeners.gitlab;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.exceptions.PersonExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabMembert2ChangeImplementerMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabProject2SourceRepositoryFromMemberMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.services.PersonService;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.filters.GitLabMember2MemberFilter;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabMember2MemberMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;
@Slf4j
@RequiredArgsConstructor

@Service
@Transactional
public class GitLabMember2MemberListener {


    @Autowired
    private GitLabMember2MemberFilter filter;

    @Autowired
    private GitLabMember2MemberMapper personMapper;

    @Autowired
    private GitLabProject2SourceRepositoryFromMemberMapper sourceRepositoryMapper;

    @Autowired
    private GitLabMembert2ChangeImplementerMapper changeImplementerMapper;

    @Autowired
    private PersonService service;

    private final KafkaTemplate<String, String> kafkaTemplate;
    /** Funcionando **/
    //Criando uma Pessoa tomando como base que ela é membra de um projeto
    @KafkaListener(topics = "application.gitlab.member", groupId = "gitlabmember2member-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, personMapper,changeImplementerMapper,sourceRepositoryMapper, false);
            }

        }

        catch (TeamExceptionNotFound e ){

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("type", "TeamExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }

        catch (PersonExceptionNotFound e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "PersonExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }

        catch (SourceRepositoryExceptionNotFound e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "SourceRepositoryExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }


        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.member.error", jsonString);
        }
    }
}
