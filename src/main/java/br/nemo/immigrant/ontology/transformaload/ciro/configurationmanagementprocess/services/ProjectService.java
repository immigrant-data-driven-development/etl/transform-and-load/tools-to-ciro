
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;

import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.ProjectApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.SourceRepositoryApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProjectService {


    @Autowired
    private ProjectApplication application;

    public void processOntology(ConsumerRecord<String, String> payload,Mapper<SoftwareProject> mapper) throws Exception {


        SoftwareProject instance = mapper.map(payload.value());
        Boolean exists = application.exists(instance.getInternalId());
        if (!exists){
            application.createOntology (instance,payload.value());

        }

    }

}
