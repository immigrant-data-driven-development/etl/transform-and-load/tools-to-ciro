  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class ProjectExceptionNotFound extends RuntimeException{

    public ProjectExceptionNotFound(String message) {
        super(message);
    }
}

