    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers;

    import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.Branch;

    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;
    import java.time.LocalDateTime;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.entity.base.models.Application;

    @Component
    public class GitLabBranch2BranchMapper  implements Mapper <Branch>{

    public Branch map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = StringUtil.check(rootNode.path("name").asText());

        Boolean merged = Boolean.parseBoolean(rootNode.path("merged").asText());

        Boolean main = Boolean.parseBoolean(rootNode.path("default").asText());

        Boolean branchprotected = Boolean.parseBoolean(rootNode.path("protected").asText());

        String weburl = StringUtil.check(rootNode.path("web_url").asText());

        String project_id = StringUtil.check(rootNode.path("project").path("id").asText());

        String externalid = new DigestUtils("SHA3-256").digestAsHex(name+project_id);

        String internalid = externalid;

        LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("commit").path("created_at").asText());

        Application application = ApplicationUtil.create(externalid,internalid,"gitlab");

        Branch branch =  Branch.builder().name(name).
                                merged(merged).
                                main(main).
                                branchprotected(branchprotected).
                                webURL(weburl).
                                createdDate(createdDate).
                                artifacttype(ArtifactType.INFORMATIONITEM).
                                internalId(internalid).build();
        branch.getApplications().add(application);

        return branch;
    }
}
