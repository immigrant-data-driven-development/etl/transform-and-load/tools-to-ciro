  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class TeamExceptionNotFound extends RuntimeException{

    public TeamExceptionNotFound(String message) {
        super(message);
    }
}

