
package br.nemo.immigrant.ontology.transformaload.ciro.teams.services;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ChangeImplementer;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.ChangeImplementerApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.SourceRepositoryApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.applications.PersonApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.exceptions.PersonExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabMember2MemberMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class PersonService {


    @Autowired
    private PersonApplication personApplication;

    @Autowired
    private ChangeImplementerApplication changeImplementerApplication;

    @Autowired
    private SourceRepositoryApplication sourceRepositoryApplication;


    public void process(ConsumerRecord<String, String> payload, Mapper<Person> personMapper, Mapper<ChangeImplementer> changeImplementerMapper, Mapper<SourceRepository> sourceRepositoryMapper, Boolean commit) throws TeamExceptionNotFound, PersonExceptionNotFound, SourceRepositoryExceptionNotFound, Exception{


        Person person = personMapper.map(payload.value());
        ChangeImplementer changeImplementer = changeImplementerMapper.map(payload.value());
        SourceRepository sourceRepository = sourceRepositoryMapper.map(payload.value());

        Boolean exists = sourceRepositoryApplication.existsByInternalId(sourceRepository.getInternalId());

        if (!exists) {

            sourceRepositoryApplication.create(sourceRepository);
        }

        exists = personApplication.existsByInternalId(person.getInternalId());

        if (!exists){
            personApplication.create(person);
        }

        exists = changeImplementerApplication.existsByInternalID(changeImplementer.getInternalId());

        if (!exists){
            changeImplementerApplication.createFromTools(changeImplementer, commit, payload.value());
        }


    }

}
