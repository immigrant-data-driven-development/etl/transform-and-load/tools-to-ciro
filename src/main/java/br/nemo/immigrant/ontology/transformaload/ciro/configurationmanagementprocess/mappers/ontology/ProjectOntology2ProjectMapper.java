    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.ontology;

    import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.springframework.stereotype.Component;

    import java.time.LocalDateTime;
    import java.util.Date;

    @Component
    public class ProjectOntology2ProjectMapper implements Mapper<SoftwareProject> {

    public SoftwareProject map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("payload").path("after").path("name").asText();

        String description = rootNode.path("payload").path("after").path("description").asText();

        //LocalDateTime startdate = DateUtil.createLocalDateTimeZ(rootNode.path("payload").path("after").path("start_date").asText());

        //LocalDateTime enddate = DateUtil.createLocalDateTimeZ(rootNode.path("payload").path("after").path("end_date").asText());

        String externalid = rootNode.path("payload").path("after").path("external_id").asText();

        String internalid = rootNode.path("payload").path("after").path("internal_id").asText();


        return SoftwareProject.builder().name(name).
                                 description(description).
                                 //createdDate(startdate).
                                 //startDate(enddate).
                                 //externalId(externalid).
                                 internalId(internalid).build();
    }
}
