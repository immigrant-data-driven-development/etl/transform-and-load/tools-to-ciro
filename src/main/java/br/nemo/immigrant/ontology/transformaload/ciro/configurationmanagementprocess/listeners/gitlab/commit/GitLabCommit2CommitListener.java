
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.listeners.gitlab.commit;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabCommit2CheckingMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabCommit2BranchMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services.CommitService;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.filters.GitLabCommitFromBranch2CommitFilter;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabCommit2CommitMapper;

import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabCommit2ChangeImplementerMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabCommit2MemberMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.GitLabProject2SourceRepositoryFromMemberMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class GitLabCommit2CommitListener {


    @Autowired
    private GitLabCommitFromBranch2CommitFilter filter;

    @Autowired
    private GitLabCommit2CommitMapper commitMapper;

    @Autowired
    private GitLabCommit2ChangeImplementerMapper changeImplementerMapper;

    @Autowired
    private GitLabCommit2MemberMapper personMapper;

    @Autowired
    private GitLabProject2SourceRepositoryFromMemberMapper projectMapper;

    @Autowired
    private GitLabCommit2BranchMapper branchMapper;

    @Autowired
    private GitLabCommit2CheckingMapper checkingMapper;

    @Autowired
    private CommitService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Funcinonando***/
    /** Criando commits baseados no commit do gitlab **/
    @KafkaListener(topics = "application.gitlab.commit", groupId = "gitlabcommitfrombranch2commit-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{


            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, commitMapper,projectMapper, branchMapper, checkingMapper, personMapper, changeImplementerMapper);

            }


        }

        catch (Exception e ){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.gitlab.commit.error", jsonString);
        }
    }
}
