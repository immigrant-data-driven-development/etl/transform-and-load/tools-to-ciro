  package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions;

  public class SpecificProcessExceptionNotFound extends RuntimeException{

    public SpecificProcessExceptionNotFound(String message) {
        super(message);
    }
}

