package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications;


import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import br.nemo.immigrant.ontology.entity.spo.project.repositories.SoftwareProjectRepository;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.ProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.DataSearch;
import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Component
@Transactional
public class ProjectApplication  {

    @Autowired
    private SoftwareProjectRepository repository;

    @Autowired
    private MongoApplication mongoApplication;

    protected Document retrieveDocument(DataSearch data) throws MongoNotFound {
        List<Document> documents = mongoApplication.find(data.getTable(),
                data.getElementValue(),
                data.getDatabase());
        return documents.get(0);
    }

    public SoftwareProject createOntology(SoftwareProject instance, String payload) throws  Exception, MongoNotFound, ProjectExceptionNotFound {

        return this.create(instance);
    }


    public SoftwareProject create(SoftwareProject project) {
        return this.repository.save(project);
    }


    public SoftwareProject retrieveByInternalID(String internalID) throws ProjectExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new ProjectExceptionNotFound(internalID));

        return createInstance(projection);
    }


    private SoftwareProject createInstance(IDProjection projection){
        return  SoftwareProject.builder().id(projection.getId()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (String internalID){
        return this.repository.existsByInternalId(internalID);
    }
}