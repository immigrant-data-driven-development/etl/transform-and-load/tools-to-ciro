
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.listeners.gitlab.branch;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabBranchCommit2CommitMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services.BranchService;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.filters.GitLabBranch2BranchFilter;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers.GitLabBranch2BranchMapper;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SourceRepositoryExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.teams.mappers.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;
@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class GitLabBranch2BranchListener {


    @Autowired
    private GitLabBranch2BranchFilter filter;

    @Autowired
    private GitLabBranch2BranchMapper branchMapper;

    @Autowired
    private GitLabProject2SourceRepositoryFromMemberMapper sourceRepositoryMapper;

    @Autowired
    private GitLabBranchCommit2CommitMapper commitMapper;

    @Autowired
    private GitLabBranch2MemberMapper personMapper;

    @Autowired
    private GitLabBranch2ChangeImplementerMapper changeImplementerMapper;

    @Autowired
    private BranchService service;

    private final KafkaTemplate<String, String> kafkaTemplate;
    /** Funcionando **/
    /** Criando uma branch baseado no conceito de Branch do Gitlab e relaciona com o sourcerepositorio **/
    @KafkaListener(topics = "application.gitlab.branch", groupId = "gitlabbranch2branch-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, branchMapper, sourceRepositoryMapper,commitMapper,personMapper,changeImplementerMapper);

            }

        }
        catch (SourceRepositoryExceptionNotFound e){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "SourceRepositoryExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.gitlab.branch", payload.value());
            kafkaTemplate.send("application.gitlab.branch.error", jsonString);

        }

        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();


            kafkaTemplate.send("application.gitlab.branch.error",jsonString);
            
        }
    }
}
