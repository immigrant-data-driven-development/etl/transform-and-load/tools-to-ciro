
package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.services;
import  br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;

import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.CheckinApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.exceptions.SpecificProcessExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;

import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
import br.nemo.immigrant.ontology.transformaload.ciro.util.mongo.MongoNotFound;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class CheckinService {


    @Autowired
    private CheckinApplication application;

    public void process(ConsumerRecord<String, String> payload,Mapper<Checkin> mapper) throws  Exception, MongoNotFound, SpecificProcessExceptionNotFound {


        Checkin instance = mapper.map(payload.value());
        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(payload.value());

        String projectExternalID = StringUtil.check(rootNode.path("project_id").asText());

        if (projectExternalID.isEmpty() || projectExternalID.isBlank()){
            projectExternalID = StringUtil.check(rootNode.path("project").asText());
        }

        Boolean exists = application.existsbyExternalId(instance.getExternalId("gitlab"));
        if (!exists){
            application.create (instance,projectExternalID);

        }

    }

}
