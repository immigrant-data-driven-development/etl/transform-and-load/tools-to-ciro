
package br.nemo.immigrant.ontology.transformaload.ciro.integration.listeners;

import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.ConfiguationManagementProcess;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.models.SourceRepository;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.ConfiguationManagementProcessRepository;
import br.nemo.immigrant.ontology.entity.cmpo.configuationmanagementprocess.repositories.SourceRepositoryRepository;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Project;
import br.nemo.immigrant.ontology.entity.spo.process.models.GeneralProjectProcess;
import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessArtifact;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.GeneralProjectProcessRepository;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessArtifactRepository;
import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import br.nemo.immigrant.ontology.entity.spo.project.repositories.SoftwareProjectRepository;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.ProjectApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.applications.SourceRepositoryApplication;
import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class IntegrateProjectBranchListener {

    @Autowired
    private SourceRepositoryApplication sourceRepositoryApplication;

    @Autowired
    private ProjectApplication projectApplication;

    @Autowired
    private GeneralProjectProcessRepository generalProjectProcessRepository;

    @Autowired
    private ConfiguationManagementProcessRepository configuationManagementProcessRepository;

    @Autowired
    private SpecificProjectProcessArtifactRepository specificProjectProcessArtifactRepository;

    private final KafkaTemplate<String, String> kafkaTemplate;

    @KafkaListener(topics = "ontology.ciro.integration", groupId = "ontology-ciro-integration-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try {

            String data = payload.value();

            ObjectMapper objectMapper = new ObjectMapper();

            JsonNode rootNode = objectMapper.readTree(data);

            String projectInternalID = StringUtil.check(rootNode.path("project").asText());


            SoftwareProject softwareProject = projectApplication.retrieveByInternalID(projectInternalID);


            //Criar o Processo Generico
            GeneralProjectProcess generalProjectProcess = GeneralProjectProcess.builder().softwareproject(softwareProject).name(softwareProject.getName()).internalId(softwareProject.getInternalId()).build();
            //generalProjectProcess.getApplications().add(generalProcessApplication);
            generalProjectProcess = generalProjectProcessRepository.save(generalProjectProcess);

            ConfiguationManagementProcess configuationManagementProcess = ConfiguationManagementProcess.builder().generalprojectprocess(generalProjectProcess).name(softwareProject.getName()).internalId(softwareProject.getInternalId()).build();
            //configuationManagementProcess.getApplications().add(specificProcessApplication);
            configuationManagementProcess = configuationManagementProcessRepository.save(configuationManagementProcess);

            JsonNode repositories =  rootNode.path("repository");

            for (JsonNode fileNode: repositories) {

                String respositoryInternalId = fileNode.asText();
                SourceRepository sourceRepository = sourceRepositoryApplication.retrieveByInternalID(respositoryInternalId);
                //Associar o repositorio ao processos especifico
                SpecificProjectProcessArtifact specificProjectProcessArtifact = SpecificProjectProcessArtifact.builder().artifact(sourceRepository).specificprojectprocess(configuationManagementProcess).eventDate(sourceRepository.getCreatedDate()).build();
                specificProjectProcessArtifactRepository.save(specificProjectProcessArtifact);


                log.info(respositoryInternalId);
            }

        } catch (Exception e) {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            //kafkaTemplate.send("ontology.ciro.integration.error", jsonString);

        }
    }
}


