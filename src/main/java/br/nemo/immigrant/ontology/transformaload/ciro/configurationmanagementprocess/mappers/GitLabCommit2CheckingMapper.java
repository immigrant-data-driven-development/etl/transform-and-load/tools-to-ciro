    package br.nemo.immigrant.ontology.transformaload.ciro.configurationmanagementprocess.mappers;

    import  br.nemo.immigrant.ontology.entity.cmpo.checkin.models.Checkin;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.ciro.util.StringUtil;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;
    import java.time.LocalDateTime;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    @Component
    public class GitLabCommit2CheckingMapper  implements Mapper <Checkin>{

    public Checkin map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = StringUtil.check(rootNode.path("title").asText());

        String externalid = StringUtil.check(rootNode.path("id").asText());

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);

        LocalDateTime createdDate = DateUtil.createLocalDateTimeZ(rootNode.path("created_at").asText());

        Application application = ApplicationUtil.create(externalid,internalid,"gitlab");

        Checkin checkin =  Checkin.builder().name(name).createdDate(createdDate).internalId(internalid).build();

        checkin.getApplications().add(application);

        return checkin;

    }
}
